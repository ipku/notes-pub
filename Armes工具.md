**website**

-   [北大毕业论文](http://www.lib.pku.edu.cn/portal/zy/dzzy/xueweilunwen)
-   [逆地理解析地图](http://lbs.amap.com/console/show/picker)
-   [北京市社会保险个人权益记录](http://www.bjrbj.gov.cn/csibiz/indinfo/login.jsp)
-   doodle
-   skillpage
-   glassdoor
-   quora digest
-   geeksforgeeks
-   Weblio #日语例句辞典
-   WordItOut #文字云
-   Compilr #云开发
-   Polarr #chrome based Photoshop
-   法国文化在中国
-   Github Pages supports HTML5 and CSS3
-   gaiagps #kml轨迹
-   [style2markdown](https://stackedit.io/app#)

  

**software**

-   Teamviewer #远程桌面
-   TeXworks #CTeX自带，预览方便
-   Pandoc #Tex/md2html等等互相各种转换
-   Poderosa #neat securecrt/putty
-   Mendeley、Zotero；中文替代(win)：docFetcher/FileLocator + highlight all search pdf acrobat
-   slack、IRC，团队开发替代微信
-   bokeh #python交互式可视化工具
-   sourcetrail #c++/java项目代码graph关系

  

**media**

-   Le Monde.fr
-   Le Figaro.fr
-   Der Tag - SPIEGEL ONLINE
-   日语表达句型
    

  

**ladder**

-   gae
-   xx-net
-   ssfree.club
-   [Teredo 通道](https://blog.felixc.at/2010/04/install-teredo-ipv6/)(ipv6,  [mac](https://blog.csdn.net/pxonly/article/details/79251274)，[set server](https://lists.afrinic.net/pipermail/afripv6-discuss/2007/000080.html))
-   vps服务([ipv6](https://www.polarxiong.com/archives/%E6%90%AD%E5%BB%BAipv6-VPN-%E8%AE%A9ipv4%E4%B8%8Aipv6-%E4%B8%8B%E8%BD%BD%E9%80%9F%E5%BA%A6%E6%8F%90%E5%8D%87%E5%88%B0100M.html)、Linode、Banwagon)
-   申请IPv6 Tunnel Broker？


